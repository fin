require 'tabdelimited'

################################################################################
# A collection of transactions.
################################################################################
class Ledger
  
  # Initializes a _Transactions_ object.
  def initialize(filename)
    @filename = filename
    @transactions = []
    @account_register = AccountRegister.new
    sync :write => false
  end

  # Synchronizes the the data store with the data store source.
  def sync(options = {})
    options = { :read => true, :write => true }.merge options
    import if options[:read]
    export if options[:write]
  end

  def length
    @transactions.length
  end

  alias size length

  def each
    @transactions.each { |transaction| yield(transaction) }
  end

  # Add one or more transactions to the ledger.
  def add(*transactions)
    transactions.each do |transaction|
      transaction.from.balance -= transaction.amount
      transaction.to.balance += transaction.amount
    end
    @transactions.push(*transactions)
  end

  # Accumulate transactions corresponding to a given predicate block.
  def accumulate
    register = AccountRegister.new # fresh register with 0 balances
    @transactions.each do |transaction|
      next unless yield(transaction)
      from = register.open(*transaction.from.path)
      from.balance -= transaction.amount
      to = register.open(*transaction.to.path)
      to.balance += transaction.amount
    end
    register.accounts
  end

  # Get a collection of the accounts defined.
  def accounts
    @account_register.accounts
  end

  private
  include TabDelimited
end
