require 'date'

################################################################################
# A financial transaction.
################################################################################
class Transaction
  attr_reader :id, :from, :to, :amount, :date, :description
  attr_writer :from, :to, :amount, :date, :description
  @@id_gen = 0

  def initialize(*transaction_values)
    from, to, amount, date, description = transaction_values
    @id = @@id_gen
    @@id_gen = @@id_gen.succ
    @from, @to, @amount, @date, @description = transaction_values
  end

  # Test for equal id of this _Transaction_ against another.
  def ==(other)
    @id == other.id
  end

  # Check whether this object's class is the same as that of the given parameter
  # and that this transaction's id is the same as that of the given parameter.
  def eql?(other)
    self.class.eql?(other.class) and @id.eql?(other.id)
  end

  # Return a unique hash for this transaction, based on its id.
  def hash
    # the hash method is called by functions such as Array.uniq.  We consider
    # a Transaction's identity by looking at its id.
    @id.hash
  end

  # Comparison operator.  Returns -1, 0 or 1 depending on whether this
  # _Transaction_ object's compares less than, equally or greater than the given
  # _Transaction_ object's date.
  def <=>(other)
    @date <=> other.date
  end
end
