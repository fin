################################################################################
# A financial account from which and to which transactions are made.
#
# _Account_ objects are both flyweights and a flyweight factory.  Since there
# is only one kind of flyweight, there is no need to increase complexity with
# an extra class.
################################################################################
class Account
  attr_reader :balance, :parent, :children

  # Initializes a new _Account_ object given the account's full path in the
  # accounts hierarchy, from the top-most parent to the account's name.
  #
  # +auto_repair_account = Account.new *['Expenses', 'Auto', 'Repair']
  # groceries_account = Account.new(*'Expenses.Groceries'.split('.'))+
  def initialize(name, parent)
    @name = name
    @parent = parent
    @children = []
    @balance = 0
  end

  # Returns the terminal name of this account.
  #
  # +account = Account.new(*'Income.Bursaries.Government'.split('.'))
  # account.name  => 'Government'+
  def name
    @name
  end

  # The string representation of the account.
  #
  # +account = AccountRegister.new.open('Income', 'Bursaries', 'Uncle Joe')
  # account.to_s  => 'Income.Bursaries.Uncle Joe'
  def to_s
    return @name if @parent.nil?
    @parent.to_s + '.' + @name
  end

  def to_sym
    to_s.to_sym
  end

  # Returns this account's depth in the accounts' hierarchy.  A depth of 0 means
  # this account is a root account.
  def depth
    return 0 if @parent.nil?
    1 + @parent.depth
  end

  # Invoke block on each of the account's parents.
  #
  # account = Account.new(*['Expenses', 'Groceries'])
  # account.each_parent { |parent| puts parent }                => Expenses
  # 
  # account.each_parent { |parent| puts parent }  => Expenses
  #                                                                Groceries
  def each_parent(&block)
    return if @parent.nil?
    yield(parent)
    @parent.each_parent(&block)
  end

  # Test for equal id of this _Transaction_ against another.
  def ==(other)
    to_s == other.to_s
  end

  # Check whether this object's class is the same as that of the given parameter
  # and that this transaction's id is the same as that of the given parameter.
  def eql?(other)
    self.class.eql?(other.class) and to_s.eql?(other.to_s)
  end

  def hash
    # the hash method is called by functions such as Array.uniq.  We consider
    # a Transaction's identity by looking at its id.
    to_s.hash
  end

  # Comparison operator
  def <=>(other)
    to_s <=> other.to_s
  end

  # Set a new value for this account's balance, and update this accont's parents'
  # balance as well.
  def balance=(new_balance)
    difference = new_balance - @balance
    @balance += difference
    @parent.balance += difference unless @parent.nil?
  end

  # Get the path to the account, from the root parent to the account's literal
  # name.
  #
  # The path is an array.  For example,
  #
  # +account = AccountRegister.open(*%w[Income Salary])
  # account.path => ["Income", "Salary"]+
  def path
    to_s.split('.')
  end
end
