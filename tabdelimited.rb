require 'transaction'
require 'accountregister'

################################################################################
# Exports transactions to and imports transactions from tab-delimited input.
################################################################################
module TabDelimited

  def import
    File.open(@filename) do |file|
      file.each_line do |line|
        line.chomp!
        next if line =~ /^#|^$/ # ignore blank, comment lines
        add(import_transaction(line))
      end
    end
  end

  def export
    File.open(@filename, "w") do |file|
      each do |transaction|
        file.puts(export_transaction)
      end
    end
  end

  private
  def import_transaction(line)
    transaction_values = line.split(/\t/, -1) # [from, to, amount, date, desc]
    from, to, amount, date, description = transaction_values
    from = @account_register.open(*from.split('.'))
    to = @account_register.open(*to.split('.'))
    amount = amount.to_f
    date = Date.parse(date)
    Transaction.new(from, to, amount, date, description)
  end

  def export_transaction(transaction)
    exported_transaction = [
                            transaction.from,
                            transaction.to,
                            transaction.amount,
                            transaction.date,
                            transaction.description
                           ].join("\t")
  end
end
