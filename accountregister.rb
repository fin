require 'account'

################################################################################
# A flyweight-factory of _Account_ objects.
################################################################################
class AccountRegister

  def initialize
      @accounts = {} # all Account objects known
  end
  
  # Open the given account.  If the account doesn't exist, it will be created.
  # The parameters should be the account's path in the hierarchy.  This method
  # represents the flyweight factory.
  #
  # +account = Account.open('Income', 'Bursaries', 'Uncle Joe')+
  def open(*path)
    error = "wrong number of arguments (0 for at least 1)"
    raise ArgumentError, error if path.empty?
    account_symbol = path.join('.').to_sym
    unless @accounts.include? account_symbol # add new account, inform parents
      parent = path[0..-2].empty? ? nil : open(*path[0..-2])
      new_account = Account.new(path[-1], parent)
      new_account.parent.children << new_account unless new_account.parent.nil?
      @accounts[account_symbol] = new_account
    end
    @accounts[account_symbol]
  end

  # Check whether the register knows about the given account.
  def include?(account)
    @accounts.include? account
  end

  # Invoke a block on each account known.
  def each
    @accounts.each_value { |account| yield(account) }
  end

  # Get the collection of accounts known.
  def accounts
    @accounts.values
  end
end
