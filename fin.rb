#!/usr/bin/ruby -w

require 'ledger'
require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage:  fin [options]"

  options[:transactions_file] = "#{File.expand_path('~')}/.fin/transactions"  
  opts.on("", "--transactions-file FILE",
          "Use transactions file FILE") do |filename|
    options[:transactions_file] = File.expand_path(filename)
  end

  options[:totals_filter] = 'true'
  opts.on("", "--filter FILTER",
          "Use Ruby predicate FILTER as filter on input.",
          "Assume availability of Transaction object",
          "parameter named \"transaction\"") do |filter|
    options[:totals_filter] = filter
  end
end.parse!

begin
  ledger = Ledger.new(options[:transactions_file])
rescue Errno::ENOENT
  STDERR.puts "Error:  Couldn't open file #{options[:transactions_file]}"
  exit 1
end

STDERR.puts "#{ledger.length} transactions read."

def totals(ledger, options = {})
  options = {:format => :humane }.merge options
  # set up a few handy names for filtering by the --filter command-line option
  yesterday, today, tomorrow = Date.today - 1, Date.today, Date.today + 1
  last_month, this_month, next_month = today << 1, today, today >> 1
  last_year, this_year, next_year = today << 12, today, today >> 12
  last_month_start = Date.new(last_month.year, last_month.month, 1)
  last_month_end = Date.new(last_month.year, last_month.month, -1)
  this_month_start = Date.new(this_month.year, this_month.month, 1)
  this_month_end = Date.new(this_month.year, this_month.month, -1)
  next_month_start = Date.new(next_month.year, next_month.month, 1)
  next_month_end = Date.new(next_month.year, next_month.month, -1)
  last_year_start = Date.new(last_year.year, 1, 1)
  last_year_end = Date.new(last_year.year, -1, -1)
  this_year_start = Date.new(this_year.year, 1, 1)
  this_year_end = Date.new(this_year.year, -1, -1)
  next_year_start = Date.new(next_year.year, 1, 1)
  next_year_end = Date.new(next_year.year, -1, -1)
  # get filtered totals.
  account_totals = ledger.accumulate do |transaction|
    from, to = transaction.from, transaction.to
    amount = transaction.amount
    date = transaction.date
    notes = transaction.description
    eval(options[:totals_filter])
  end
  # sort totals
  account_totals.sort! do |account1, account2|
    account1.to_s <=> account2.to_s
  end

  # print totals out
  STDERR.puts "#{account_totals.length} accounts defined.", "\n"
  account_totals.each_index do |index|
    if options[:format] == :humane
      account_name = '  ' * (account_totals[index].depth) + account_totals[index].name
      print account_name
      print(("%.2f" % account_totals[index].balance).rjust(40 - account_name.length, '.'))
    else
      print account
      print("\t", ("%.2f" % account_totals[index]))
    end
    puts
  end
end

totals ledger, options.merge({:format => :humane})
